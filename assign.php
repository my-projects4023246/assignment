<?php
    
    echo "<html>
            <head>
                <link rel='stylesheet' href='bootstrap/bootstap.css'>
                <link rel='stylesheet' href='style.css'>
                <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'> 
            </head>
            <body>
                <div class='container mt-3'>
                    <ul class='nav nav-pills'>
                            <li class='nav-item' id='sli'>
                                <a class='nav-link text-white bg-secondary' href='http://localhost/assignment2/assignment.html'><i class='fa fa-arrow-left'></i> Back </a>
                            </li>
                    </ul>
                </div>
            </body>
        </html>";

        // if (isset($_GET['catch'])) {
            $num1 = $_GET['num1'];
            $num2 = $_GET['num2'];
            $math = $_GET['math'];
        
            echo "<div class='alert' role='alert' id='question'>
                    <h4 class='alert-title'>Assignment 2</h4>
                    <p>Collect input for 2 numbers and use buttons for each of addition, subtraction, multiplication and division</p>
                    <hr>
                    <div class='container'>
                        <div class='form-group'>
                            <label for=''>1st number: </label>
                            <small class='form-text text-muted'>$num1</small>
                        </div>
                        <div class='form-group'>
                            <label for=''>2nd number: </label>
                            <small class='form-text text-muted'>$num2</small>
                        </div>
                        <hr>
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='card text-center'>
                                    <div class='card-body'>
                                        <h5 class='card-title'>Result</h5>";
                                        if ($math == '+') {
                                            $add = $num1 + $num2;
                                            echo "$num1 + $num2 = " .$add;
                                        }
                                        elseif ($math == '-') {
                                            $sub = $num1 - $num2;
                                            echo "$num1 - $num2 = " .$sub;
                                        }
                                        elseif ($math == 'x') {
                                            $mul = $num1 * $num2;
                                            echo "$num1 x $num2 = " .$mul;
                                        }
                                        elseif ($math == '/') {
                                            if ($num2 == 0) {
                                                echo "ERROR: The denominator cannot be zero.";
                                            }
                                            else {
                                                $div = $num1 / $num2;
                                                echo "$num1 / $num2 = " .$div;
                                            }
                                        }
                                                        

        echo "                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
            
    
        // }

        // else {
        //     echo "ERROR";
        // }

   

   
?>